package treeprint

import (
	"fmt"
	"io"
)

type Tree interface {
	GetChildren() []Tree
	StringValue() string
}

func Simple(t Tree) string {
	return simpleHelper(t, "", true)
}

func DotTree(t Tree, w io.Writer) {
	w.Write([]byte("hello world"))
}

// Writes to given writer a dot output with given tree (TREE NODES MUST RETURN UNIQUE STRING VALUES)
// otherwise the dot graph will look weird. This is not up to me, this is how dot works
func DotBinaryTree(t BinaryTree, w io.Writer) {
	dotHeader(w)
	dotBinaryTreeHelper(t, w, 0)
	fmt.Fprintf(w, "}\n") // tail
}
func dotHeader(w io.Writer) {
	fmt.Fprintf(w, "digraph BST {\n")
	fmt.Fprintf(w, "    node [fontname=\"Arial\"];\n")
}
func dotNilNode(w io.Writer, val string, nils int) {
	fmt.Fprintf(w, "    null%d [shape=point];\n", nils)
	fmt.Fprintf(w, "    %s -> null%d;\n", val, nils)
}
func dotBinaryTreeHelper(t BinaryTree, w io.Writer, nils int) int {
	left, right := t.GetLeft(), t.GetRight()
	if left != nil {
		fmt.Fprintf(w, "    %s -> %s;\n", t.StringValue(), left.StringValue())

	} else {
		nils += 1
		dotNilNode(w, t.StringValue(), nils)
	}

	if right != nil {
		fmt.Fprintf(w, "    %s -> %s;\n", t.StringValue(), right.StringValue())

	} else {
		nils += 1
		dotNilNode(w, t.StringValue(), nils)
	}
	// recurse after because it makes graph look nicer
	if left != nil {
		nils = dotBinaryTreeHelper(left, w, nils)
	}
	if right != nil {
		nils = dotBinaryTreeHelper(right, w, nils)
	}
	return nils
}

func simpleHelper(t Tree, prefix string, isTail bool) string {
	ternary := func(b bool, s1, s2 string) string {
		if b {
			return s1
		}
		return s2
	}
	s := prefix + ternary(isTail, "└── ", "├── ") + t.StringValue() + "\n"
	children := t.GetChildren()
	if len(children) > 0 {
		for _, child := range children[:len(children)-1] {
			s += simpleHelper(child, prefix+ternary(isTail, "    ", "│   "), false)
		}
		child := children[len(children)-1]
		s += simpleHelper(child, prefix+ternary(isTail, "    ", "│   "), true)
	}
	return s
}

type BinaryTree interface {
	GetLeft() BinaryTree
	GetRight() BinaryTree
	StringValue() string
}
