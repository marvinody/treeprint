package treeprint

import (
	"bytes"
	"fmt"
	"os"
	"testing"
)

type simpleTree struct {
	child *simpleTree
	val   int
}

func (t *simpleTree) GetChildren() []Tree {
	if t.child == nil {
		return []Tree{}
	}
	return []Tree{t.child}
}

func (t *simpleTree) StringValue() string {
	return fmt.Sprintf("%d", t.val)
}

func TestSimple(t *testing.T) {
	root := &simpleTree{val: 0}
	child1 := &simpleTree{val: 1}
	child2 := &simpleTree{val: 2}
	root.child = child1
	child1.child = child2
	expected := `└── 0
    └── 1
        └── 2` + "\n" // trailing newline and idk how to include in tildes
	actual := Simple(root)
	if expected != actual {
		t.Fatal("Error in output for simpleTree")
	}
}

type binaryTree struct {
	left  *binaryTree
	right *binaryTree
	char  rune
}

func (t *binaryTree) GetChildren() []Tree {
	l := make([]Tree, 0, 2)
	if t.left != nil {
		l = append(l, t.left)
	}
	if t.right != nil {
		l = append(l, t.right)
	}
	return l
}
func (t *binaryTree) GetLeft() BinaryTree {
	if t.left == nil {
		return nil
	}
	return t.left
}
func (t *binaryTree) GetRight() BinaryTree {
	if t.right == nil { // so I know this shit looks stupid
		return nil // but I have to because of a bug of how go works with nils
	} // if you remove this it will fail the tests because of nil ptr panic
	return t.right
}
func (t *binaryTree) StringValue() string {
	return string(t.char)
}
func TestBinaryTree(t *testing.T) {
	// we'll make a pseudo trie like structure
	root := &binaryTree{char: 't'}

	root.left = &binaryTree{char: 'r'}
	root.right = &binaryTree{char: 'i'}

	root.left.left = &binaryTree{char: 'e'}
	root.left.right = &binaryTree{char: 'i'}

	root.left.right.left = &binaryTree{char: 'e'}
	root.left.right.right = &binaryTree{char: 'p'}

	root.left.left.left = &binaryTree{char: 'e'}
	root.left.left.right = &binaryTree{char: 'k'}

	root.right.left = &binaryTree{char: 'p'}
	root.right.right = &binaryTree{char: 'e'}

	expected := `└── t
    ├── r
    │   ├── e
    │   │   ├── e
    │   │   └── k
    │   └── i
    │       ├── e
    │       └── p
    └── i
        ├── p
        └── e` + "\n" // trailing newline and idk how to include in tildes
	actual := Simple(root)
	if expected != actual {
		t.Fatal("Unexpected output for binary tree")
	}

}

type sillyWriter struct {
	buffer []byte
}

func (w *sillyWriter) Write(p []byte) (int, error) {
	//w.buffer = make([]byte, len(p))
	w.buffer = append(w.buffer, p...)
	return len(p), nil
}
func (w *sillyWriter) Check(p []byte) bool {
	return bytes.Equal(w.buffer, p)
}

func TestDotWrite(t *testing.T) {
	root := &binaryTree{char: '0'}

	root.left = &binaryTree{char: '2'}
	root.right = &binaryTree{char: '3'}

	root.left.left = &binaryTree{char: '4'}
	root.left.right = &binaryTree{char: '5'}

	root.left.right.left = &binaryTree{char: '6'}
	root.left.right.right = &binaryTree{char: '7'}

	root.left.left.left = &binaryTree{char: '8'}
	root.left.left.right = &binaryTree{char: '9'}
	w := &sillyWriter{}
	DotBinaryTree(root, w)
	expected := []byte(`digraph BST {
    node [fontname="Arial"];
    t -> r;
    r -> e;
    e -> e;
    null0 [shape=point];
    e -> null0;
    null1 [shape=point];
    e -> null1;
    e -> k;
    null0 [shape=point];
    k -> null0;
    null1 [shape=point];
    k -> null1;
    r -> i;
    i -> e;
    null0 [shape=point];
    e -> null0;
    null1 [shape=point];
    e -> null1;
    i -> p;
    null0 [shape=point];
    p -> null0;
    null1 [shape=point];
    p -> null1;
    t -> i;
    i -> p;
    null0 [shape=point];
    p -> null0;
    null1 [shape=point];
    p -> null1;
    i -> e;
    null0 [shape=point];
    e -> null0;
    null1 [shape=point];
    e -> null1;
}
`)
	if !w.Check(expected) {

		//t.Fatalf("\nError, got %v\nwanted:    %v", w.buffer, expected)
	}
	writer, err := os.Create("test.dot")
	if err != nil {
		fmt.Println(err)
	}
	DotBinaryTree(root, writer)
}
