To use, simply import and create 2 new methods `GetChildren() []Tree` and `StringValue() string` on your tree like thing

Then call the `Simple` function or another (Unimplemented as of now) and pass the root of your tree
Check the test cases for some simple examples
In the case a node has no children, just return an empty array 
